use std::{env, process};

use grep_minimal::Config;

fn main()
{
	//let args: Vec<String> = env::args().collect();
	//println!("{:?}", args);

	//let query = &args[1];
	//let filename = &args[2];
	let config: Config = Config::new(env::args()).unwrap_or_else(|err|
	{
		eprintln!("Problem parsing arguments: {}", err);
		process::exit(1);
	});
	//println!("Searching for {}", config.query);
	//println!("In file {}", config.filename);

	if let Err(e) = grep_minimal::run(config)
	{
		eprintln!("Application error: {}", e);
		process::exit(1);
	}
}
